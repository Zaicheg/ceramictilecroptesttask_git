﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityZaichegTools;

/// <summary>
/// Класс отвечает за приём событий Unity и их передачу в остальные менеджеры
/// </summary>
public class UnityEventsManager : Singleton<UnityEventsManager>
{
	/// <summary>
	/// Метод обрабатывает событие Unity -- Awake
	/// </summary>
	private void Awake()
	{
	}

	/// <summary>
	/// Метод обрабатывает событие Unity -- OnEnable
	/// </summary>
	private void OnEnable()
	{
		GUIManager.Instance.OnEnableEvent();
	}

	/// <summary>
	/// Метод обрабатывает событие Unity -- Update
	/// </summary>
	private void Update()
	{
		GUIManager.Instance.UpdateEvent();
	}
}
