﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityZaichegTools;

/// <summary>
/// Класс отвечает за работу с графическим интерфейсом пользователя
/// </summary>
public class GUIManager : Singleton<GUIManager>
{
	public ExtendedButton MoveTileLeftButton;
	public ExtendedButton MoveTileRightButton;
	public ExtendedButton MoveTileDownButton;
	public ExtendedButton MoveTileUpButton;

	public ExtendedButton RotateTileLeftButton;
	public ExtendedButton RotateTileRightButton;

	public ExtendedButton ScaleTileLessButton;
	public ExtendedButton ScaleTileMoreButton;

	public Button CloneTileButton;

	public Button CropTilesButton;
	public Button CropSubwallButton;

	/// <summary>
	/// Метод обрабатывает событие Unity - Awake
	/// </summary>
	public void OnEnableEvent()
	{
		InitUserInterfaceElements();
	}

	/// <summary>
	/// Метод обрабатывает событие Unity -- Update
	/// </summary>
	public void UpdateEvent()
	{
		CheckRepeatButtons();
	}

	/// <summary>
	/// Метод инициирует различные элементы интерфейса
	/// </summary>
	private void InitUserInterfaceElements()
	{
		CloneTileButton.onClick.AddListener(General.Instance.CloneCeramicTile);
		CropTilesButton.onClick.AddListener(General.Instance.CropTilesOnWall);
		CropSubwallButton.onClick.AddListener(General.Instance.CropSubwall);
	}

	/// <summary>
	/// Метод проверяет состояние зажимаемых кнопок
	/// </summary>
	private void CheckRepeatButtons()
	{
		if (MoveTileLeftButton.IsPressed)
		{
			General.Instance.MoveAllTilesOrHoles(new Vector2(-1f, 0f));
		}
		if (MoveTileRightButton.IsPressed)
		{
			General.Instance.MoveAllTilesOrHoles(new Vector2(1f, 0f));
		}
		if (MoveTileDownButton.IsPressed)
		{
			General.Instance.MoveAllTilesOrHoles(new Vector2(0f, -1f));
		}
		if (MoveTileUpButton.IsPressed)
		{
			General.Instance.MoveAllTilesOrHoles(new Vector2(0f, 1f));
		}

		if (RotateTileLeftButton.IsPressed)
		{
			General.Instance.RotateAllTilesOrHoles(-1f);
		}
		if (RotateTileRightButton.IsPressed)
		{
			General.Instance.RotateAllTilesOrHoles(1f);
		}

		if (ScaleTileLessButton.IsPressed)
		{
			General.Instance.ScaleAllTiles(-1f);
		}

		if (ScaleTileMoreButton.IsPressed)
		{
			General.Instance.ScaleAllTiles(1f);
		}
	}
}