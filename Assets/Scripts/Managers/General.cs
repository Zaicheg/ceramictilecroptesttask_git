﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityZaichegTools;

/// <summary>
/// Просто супер-класс
/// </summary>
public class General : Singleton<General>
{
	public Wall Wall; // Стена, в которой вырезаются проёмы
	public List<Hole> Holes = new List<Hole>(); // Проёмы, по которым подрезается плитка и которые вырезаются в стене
	public List<Thing> CeramicTiles = new List<Thing>(); // Плитки, которые подрезаются по контуру многоугольника и по контурам проёмов
	public Thing Subwall;

	#region Перемещение плиток и проёмов

	/// <summary>
	/// Метод смещает все керамические плитки или проёмы
	/// </summary>
	/// <param name="direction">Направление смещения</param>
	public void MoveAllTilesOrHoles(Vector2 direction)
	{
		if (CeramicTiles.Count > 0)
		{
			foreach (var ceramicTile in CeramicTiles)
				MoveTile(ceramicTile, direction);

			//CropTilesOrWall();
		}
	}

	/// <summary>
	/// Метод смещает керамическую плитку
	/// </summary>
	/// <param name="ceramicTile">Смещаемая плитка</param>
	/// <param name="direction">Направление смещения</param>
	private void MoveTile(Thing ceramicTile, Vector2 direction)
	{
		var targetPosition = ceramicTile.transform.position;
		targetPosition += transform.forward * direction.x * Time.deltaTime;
		targetPosition += transform.up * direction.y * Time.deltaTime;

		ceramicTile.transform.position = targetPosition;
	}

	#endregion

	#region Вращение плиток и проёмов

	/// <summary>
	/// Метод поворачивает все керамические плитки или проёмы вокруг локальной оси Х
	/// </summary>
	/// <param name="angle">Угол поворота</param>
	public void RotateAllTilesOrHoles(float angle)
	{
		if (CeramicTiles.Count > 0)
		{
			foreach (var ceramicTile in CeramicTiles)
				RotateTile(ceramicTile, angle);
		}
	}

	/// <summary>
	/// Метод поворачивает керамическую плитку вокруг локальной оси Х
	/// </summary>
	/// <param name="ceramicTile">Поворачиваемая плитка</param>
	/// <param name="angle">Угол поворота</param>
	private void RotateTile(Thing ceramicTile, float angle)
	{
		const float speed = 10f;
		ceramicTile.transform.RotateAround(transform.position, transform.right, angle * speed * Time.deltaTime);
	}

	#endregion

	#region Масштабирование плиток

	/// <summary>
	/// Метод масштабирует все плитки
	/// </summary>
	/// <param name="direction"></param>
	public void ScaleAllTiles(float direction)
	{
		foreach (var tile in CeramicTiles)
		{
			tile.transform.localScale += Vector3.one * direction * Time.deltaTime * 0.3f;
		}
	}

	#endregion

	#region Размножение плиток

	/// <summary>
	/// Метод размножает керамическую плитку в указанном количестве
	/// </summary>
	public void CloneCeramicTile()
	{
		if (CeramicTiles.Count != 1)
			return;

		var countOnOneSide = 5;
		var sourceCeramicTile = CeramicTiles[0];
		const float interval = 0.01f;

		for (var i = 0; i < 4; i++)
		{
			for (var k = 0; k < countOnOneSide; k++)
			{
				for (var m = 0; m < countOnOneSide; m++)
				{
					if (k == 0 && m == 0)
						continue;
					if ((i == 1 || i == 2) && m == 0)
						continue;
					if ((i == 2 || i == 3) && k == 0)
						continue;

					var targetPosition = sourceCeramicTile.transform.position;
					if (i == 0)
						targetPosition += sourceCeramicTile.transform.forward * (sourceCeramicTile.transform.localScale.z + interval) * k + sourceCeramicTile.transform.up * (sourceCeramicTile.transform.localScale.y + interval) * m;
					if (i == 1)
						targetPosition += sourceCeramicTile.transform.forward * (sourceCeramicTile.transform.localScale.z + interval) * k - sourceCeramicTile.transform.up * (sourceCeramicTile.transform.localScale.y + interval) * m;
					if (i == 2)
						targetPosition += -sourceCeramicTile.transform.forward * (sourceCeramicTile.transform.localScale.z + interval) * k - sourceCeramicTile.transform.up * (sourceCeramicTile.transform.localScale.y + interval) * m;
					if (i == 3)
						targetPosition += -sourceCeramicTile.transform.forward * (sourceCeramicTile.transform.localScale.z + interval) * k + sourceCeramicTile.transform.up * (sourceCeramicTile.transform.localScale.y + interval) * m;

					var instTile = Instantiate(sourceCeramicTile);
					instTile.name = "CeramicTile_" + i + "_" + k + "_" + m;
					instTile.transform.position = targetPosition;
					instTile.transform.parent = sourceCeramicTile.transform.parent;
					instTile.transform.eulerAngles = sourceCeramicTile.transform.eulerAngles;

					CeramicTiles.Add(instTile);
				}
			}
		}
	}

	#endregion

	#region Подрезка плиток

	/// <summary>
	/// Метод подрезает плитки на стене
	/// </summary>
	public void CropTilesOnWall()
	{
		if (CeramicTiles.Count > 0)
		{
			new Triangulator().CropThings(CeramicTiles, Wall, Holes, CeramicTiles[0].transform.localScale.x);
		}
	}

	/// <summary>
	/// Метод подрезает сабстену
	/// </summary>
	public void CropSubwall()
	{
		if (Subwall)
		{
			new Triangulator().CropThing(Subwall, Wall, Holes, Subwall.transform.localEulerAngles.x);
		}
	}

	#endregion
}