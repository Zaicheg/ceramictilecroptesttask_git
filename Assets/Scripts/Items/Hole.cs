﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Класс описывает дверной проём
/// </summary>
public class Hole : MonoBehaviour
{
    [SerializeField]
    private List<Transform> originContourPoints;

	/// <summary>
	/// Метод возвращает точки контура
	/// </summary>
	/// <returns></returns>
    public List<Vector3> GetOriginContourPoints()
    {
        List<Vector3> res = new List<Vector3>(originContourPoints != null && originContourPoints.Count > 0 ? originContourPoints.Count : 100);
        foreach (var t in originContourPoints)
            res.Add(t.position);
        return res;
    }
}
