﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Класс описывает стену
/// </summary>
public class Wall : MonoBehaviour
{
    public List<Transform> contourPoints = new List<Transform>();

    private Vector3? _normal;
    public Vector3 normal
    {
        get
        {
            if (_normal.HasValue)
            {
                return _normal.Value
                    ;           }
            _normal = contourPoints[0].right;
            return _normal.Value;
        }
    }

    private GameObject m_tempGO;
    private GameObject tempGO
    {
        get
        {
            if (m_tempGO!=null)
            {
                return m_tempGO;
            }
            m_tempGO = new GameObject("temp");
            m_tempGO.transform.SetParent(transform, true);

            List<Vector3> wallPoints = GetOriginContourPoints();            
            m_tempGO.transform.position = wallPoints[0];
            m_tempGO.transform.rotation = Quaternion.LookRotation(Vector3.Cross(wallPoints[0] - wallPoints[1], wallPoints[0] - wallPoints[2]));
            return m_tempGO;
        }
    }

	/// <summary>
	/// Метод возвращает точки контура стены
	/// </summary>
	/// <returns></returns>
    public List<Vector3> GetOriginContourPoints()
    {
        List<Vector3> res = new List<Vector3>(contourPoints != null && contourPoints.Count > 0 ? contourPoints.Count : 100);
        foreach (var checkpoint in contourPoints)
        {
            res.Add(checkpoint.position);
        }
        return res;
    }

    public List<Vector2> ProjectPointsToWall(List<Vector3> points)
    {
        var res = new List<Vector2>();            
        foreach (var v in points)
        {
            var localV = tempGO.transform.InverseTransformPoint(v);
            res.Add(new Vector2(localV.x, localV.y));
        }
        return res;        
    }

    public Vector3 TransformPoint(Vector2 point)
    {
        return tempGO.transform.TransformPoint(new Vector3(point.x, point.y, 0f));
    }

    public Vector3 InverseTransformPoint(Vector3 point)
    {
        return tempGO.transform.InverseTransformPoint(point);
    }

    public Vector3 InverseTransformDirection(Vector3 direction)
    {
        return tempGO.transform.InverseTransformDirection(direction);
    }

    public Vector3 TransformDirection(Vector3 direction)
    {
        return tempGO.transform.TransformDirection(direction);
    }

    public Vector3 TransformVector(Vector3 vector)
    {
        return tempGO.transform.TransformVector(vector);
    }
}
