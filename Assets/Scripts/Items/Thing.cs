﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Класс описывает штуку
/// </summary>
public class Thing : MonoBehaviour
{
	#region Поля

	public List<Transform> originContourPoints = new List<Transform>();

	public Transform contourPoint00;
	public Transform contourPoint01;
	public Transform contourPoint11;

	public CropState cropState;

	private MeshRenderer _mr;
	public MeshRenderer mr
	{
		get
		{
			if (_mr != null)
			{
				return _mr;
			}
			_mr = GetComponent<MeshRenderer>();
			return _mr;
		}
	}

	private MeshFilter _mf;
	public MeshFilter mf
	{
		get
		{
			if (_mf != null)
			{
				return _mf;
			}
			_mf = GetComponent<MeshFilter>();
			return _mf;
		}
	}

	public Mesh originalMesh;

	#endregion

	#region Методы
	
	/// <summary>
	/// Метод возвращает точки контура штуки
	/// </summary>
	/// <returns></returns>
	public List<Vector3> GetOriginContourPoints()
	{
		List<Vector3> res = new List<Vector3>(originContourPoints != null && originContourPoints.Count > 0 ? originContourPoints.Count : 100);
		foreach (var t in originContourPoints)
		{
			res.Add(t.position);
		}
		return res;
	}

	/// <summary>
	/// Метод включает/выключает отображение штуки
	/// </summary>
	/// <param name="value"></param>
	public void SetThingEnabled(bool value)
	{
		if (mr != null)
		{
			mr.enabled = value;
		}
		if (!value)
		{
			cropState = CropState.HIDDEN;
		}
	}

	/// <summary>
	/// Метод формирует подрезанный меш штуки
	/// </summary>
	/// <param name="crossedTriangles"></param>
	/// <param name="wall"></param>
	/// <param name="width"></param>
	public void CutThing(List<Triangle> crossedTriangles, Wall wall, float width)
	{
		cropState = CropState.CUTTED;
		SetThingEnabled(true);

		if (mf == null)
		{
			Debug.LogError("Cant find MeshFilter!");
			return;
		}

		List<ExtendedPoint> extPoints = new List<ExtendedPoint>();

		List<int> faceTriangles = new List<int>();
		List<Vector3> faceTrPoints = new List<Vector3>();
		List<Vector2> uvs = new List<Vector2>();
		float distanceFromWall = wall.InverseTransformPoint(GetOriginContourPoints()[0]).z;
		Vector3 correctionForThingPivot = wall.TransformDirection(new Vector3(0f, 0f, distanceFromWall));

		foreach (var tr in crossedTriangles)
		{
			int indexA = extPoints.IndexOf(tr.A);
			int indexB = extPoints.IndexOf(tr.B);
			int indexC = extPoints.IndexOf(tr.C);

			Vector3 extPA3D = Vector3.zero;
			Vector3 extPB3D = Vector3.zero;
			Vector3 extPC3D = Vector3.zero;

			if (indexA == -1)
			{
				indexA = AddFacePointToLists(tr.A, ref extPoints, wall, out extPA3D, ref faceTrPoints, correctionForThingPivot);
			}
			else
			{
				extPA3D = faceTrPoints[indexA];
			}
			if (indexB == -1)
			{
				indexB = AddFacePointToLists(tr.B, ref extPoints, wall, out extPB3D, ref faceTrPoints, correctionForThingPivot);
			}
			else
			{
				extPB3D = faceTrPoints[indexB];
			}
			if (indexC == -1)
			{
				indexC = AddFacePointToLists(tr.C, ref extPoints, wall, out extPC3D, ref faceTrPoints, correctionForThingPivot);
			}
			else
			{
				extPC3D = faceTrPoints[indexC];
			}

			///берем точки треугольника меша в локальных координатах плитки и получаем векторное произведение
			Vector3 cross = Vector3.Cross(extPB3D - extPA3D, extPC3D - extPA3D);
			//Debug.Log(wall.InverseTransformDirection(cross));
			//переводим векторное произведение в локальные координаты стены
			if (wall.InverseTransformDirection(transform.TransformDirection(cross)).z >= 0)
			{
				faceTriangles.Add(indexA);
				faceTriangles.Add(indexB);
				faceTriangles.Add(indexC);
			}
			else
			{
				faceTriangles.Add(indexA);
				faceTriangles.Add(indexC);
				faceTriangles.Add(indexB);
			}
		}

		List<Vector3> backTrPoints = new List<Vector3>(faceTrPoints.Count);
		List<int> backTriangles = new List<int>(faceTriangles.Count);
		for (int i = 0; i < faceTriangles.Count - 2; i += 3)
		{
			backTriangles.Add(faceTriangles[i] + faceTrPoints.Count);
			backTriangles.Add(faceTriangles[i + 2] + faceTrPoints.Count);//меняем очередность точек у каждого треугольникао, ибо поверность должна смотреть в другую сторону
			backTriangles.Add(faceTriangles[i + 1] + faceTrPoints.Count);
		}

		Vector3 vWidth = transform.InverseTransformVector(wall.normal * width);
		//Debug.Log("vWidth = "+ vWidth.ToString());
		for (int i = 0; i < faceTrPoints.Count; i++)
		{
			backTrPoints.Add(faceTrPoints[i] - vWidth);
		}

		List<int> sideTriangles = new List<int>();
		for (int i = 0; i < faceTriangles.Count - 2; i += 3)
		{
			int fA = faceTriangles[i];
			int fB = faceTriangles[i + 1];
			int fC = faceTriangles[i + 2];

			int bA = backTriangles[i];
			int bB = backTriangles[i + 1];
			int bC = backTriangles[i + 2];

			AddSquad(ref sideTriangles, fA, fB, bA, bC);
			AddSquad(ref sideTriangles, fA, fC, bA, bB);
			AddSquad(ref sideTriangles, fB, fC, bC, bB);
		}

		List<Vector3> resPoints = new List<Vector3>();
		List<int> resTriangles = new List<int>();
		resPoints.AddRange(faceTrPoints);
		resPoints.AddRange(backTrPoints);
		//resPoints.AddRange(backTrPoints);
		resTriangles.AddRange(faceTriangles);
		resTriangles.AddRange(backTriangles);
		//resTriangles.AddRange(backTriangles);
		resTriangles.AddRange(sideTriangles);

		GameObject tempGO = new GameObject("tempGO");
		//tempGO.transform.SetParent(transform, true);
		Transform tempGOtr = tempGO.transform;
		tempGOtr.position = contourPoint00.position;

		tempGOtr.rotation = Quaternion.LookRotation(wall.normal, (contourPoint01.position - contourPoint00.position).normalized);

		//Vector3 p00_local = tempGOtr.InverseTransformPoint(point00.position);
		Vector3 p11_local = tempGOtr.InverseTransformPoint(contourPoint11.position);
		//Vector2 p00v2 = new Vector2(p00_local.x, p00_local.y);
		//Vector2 p11v2 = new Vector2(p11_local.x, p11_local.y);
		/*
        if (Mathf.Abs(p11_local.x-p11_local.y)>0.01f)
        {
            Debug.LogError("Looks like some error with uv!!!");
        }
        */

		Vector2 uvScale = new Vector2(1 / p11_local.x, 1 / p11_local.y);

		foreach (var p in resPoints)
		{
			Vector3 localP = tempGOtr.InverseTransformPoint(transform.TransformPoint(p));
			Vector2 uv = new Vector2(localP.x * uvScale.x, localP.y * uvScale.y);
			uvs.Add(uv);
		}


		Mesh newMesh = new Mesh();
		newMesh.SetVertices(resPoints);
		newMesh.SetTriangles(resTriangles, 0);
		newMesh.SetUVs(0, uvs);

		newMesh.RecalculateNormals();

		mf.mesh = newMesh;

		//_backPointsDebug.AddRange(backTrPoints);
		Destroy(tempGO);
	}

	private void AddSquad(ref List<int> triangles, int fA, int fB, int bA, int bC)
	{
		AddTriangle(ref triangles, fA, fB, bA);
		AddTriangle(ref triangles, bA, bC, fB);
	}

	private void AddTriangle(ref List<int> triangles, int A, int B, int C)
	{
		triangles.Add(A);
		triangles.Add(B);
		triangles.Add(C);

		triangles.Add(A);
		triangles.Add(C);
		triangles.Add(B);
	}

	private int AddFacePointToLists(ExtendedPoint eP, ref List<ExtendedPoint> extPoints, Wall wall, out Vector3 point3d, ref List<Vector3> newTrPoints, Vector3 correctionForThingPivot)
	{
		extPoints.Add(eP);
		int res = extPoints.Count - 1;
		point3d = transform.InverseTransformPoint(wall.TransformPoint(eP.p) + correctionForThingPivot);
		newTrPoints.Add(point3d);
		return res;
	}

	/// <summary>
	/// Метод восстанавливает меш штуки до неподрезанного
	/// </summary>
	public void SetThingFullSize()
	{
		mf.mesh = originalMesh;
		SetThingEnabled(true);
	}

	#endregion

	#region Для отладки

	private List<Vector3> _backPointsDebug = new List<Vector3>();
	void OnDrawGizmos()
	{
		if (_backPointsDebug != null && _backPointsDebug.Count > 0)
		{
			foreach (var p in _backPointsDebug)
			{
				Gizmos.DrawWireSphere(transform.TransformPoint(p), 0.1f);
			}
		}
	}

	#endregion
}
