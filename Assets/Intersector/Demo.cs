﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Demo : MonoBehaviour {

    public PointsHolder trPHolerTriangulating;

    public PointsHolder trPHolerCol1;
    public PointsHolder trPHolerCol2;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    
    public void OnTestCollisions()
    {
        List<Vector2> pointsCol1 = GetVector2Points(trPHolerCol1);
        List<Vector2> pointsCol2 = GetVector2Points(trPHolerCol2);

        Figure f1 = new Figure(pointsCol1);
        Figure f2 = new Figure(pointsCol2);

        //Figure fRes = new Figure();
        //List<Vector2> resCol1 = new List<Vector2>();

        Figure.Intersect(f1, f2);

        Triangulator tr = new Triangulator(f1, f2);
        tr.Triangulate();        

        GameObject newGO = new GameObject("Triangles");
        int index = 0;
        foreach (var s in tr.crossedTriangles)
        {
            GameObject l = new GameObject("tr" + index.ToString());
            l.transform.SetParent(newGO.transform, true);

            GameObject p1 = new GameObject("p1");
            p1.transform.SetParent(l.transform, true);
            p1.transform.position = new Vector3(0, s.A.p.y, s.A.p.x);

            GameObject p2 = new GameObject("p2");
            p2.transform.SetParent(l.transform, true);
            p2.transform.position = new Vector3(0, s.B.p.y, s.B.p.x);

            GameObject p3 = new GameObject("p3");
            p3.transform.SetParent(l.transform, true);
            p3.transform.position = new Vector3(0, s.C.p.y, s.C.p.x);

			//PointsHolder ph = l.AddComponent<PointsHolder>();
			l.AddComponent<PointsHolder>();
			index++;
        }

        newGO.transform.position -= new Vector3(0.1f, 0, 0);

        /*        GameObject newGO = new GameObject("Triangle");
        int index = 0;
        foreach (var s in tr.triangleSegments)
        {
            GameObject l = new GameObject("line"+ index.ToString());
            l.transform.SetParent(newGO.transform, true);

            GameObject p1 = new GameObject("p1");
            p1.transform.SetParent(l.transform, true);
            p1.transform.position = new Vector3(0, s.A.p.y, s.A.p.x);

            GameObject p2 = new GameObject("p2");
            p2.transform.SetParent(l.transform, true);
            p2.transform.position = new Vector3(0, s.B.p.y, s.B.p.x);

            PointsHolder ph = l.AddComponent<PointsHolder>();
            index++;
        }

        */

        /*
        GameObject newGO2 = new GameObject("Orig");
        foreach (var s in tr.origSegments)
        {
            GameObject l = new GameObject("line");
            l.transform.SetParent(newGO2.transform, true);

            GameObject p1 = new GameObject("p1");
            p1.transform.SetParent(l.transform, true);
            p1.transform.position = new Vector3(0, s.A.p.y, s.A.p.x);

            GameObject p2 = new GameObject("p2");
            p2.transform.SetParent(l.transform, true);
            p2.transform.position = new Vector3(0, s.B.p.y, s.B.p.x);

            PointsHolder ph = l.AddComponent<PointsHolder>();
        }
        */


        //CreatePointsGO(tr.basePoints, "P_bp", Color.yellow);
        //Polygon pol = new Polygon(conterCWList.ToArray());

        CreatePointsGO(f1.points, "P_f1", Color.red);
        CreatePointsGO(f2.points, "P_f2", Color.blue);

    }

    private static void CreatePointsGO(List<Vector2> points, string name, Color color)
    {
        GameObject newGO = new GameObject(name);
        foreach (var v in points)
        {
            GameObject p = new GameObject("p");
            p.transform.SetParent(newGO.transform, true);
            p.transform.position = new Vector3(0, v.y, v.x);
        }
        PointsHolder ph = newGO.AddComponent<PointsHolder>();
        ph.color = color;
    }

    private List<Vector2> GetVector2Points(PointsHolder pHolder)
    {
        List<Vector2> pointsCol1 = new List<Vector2>();
        foreach (var p in pHolder.points)
        {
            pointsCol1.Add(new Vector2(p.position.z, p.position.y));
        }
        List<Vector2> conterCWList = new List<Vector2>(pointsCol1);
        conterCWList.Reverse();
        return conterCWList;
    }
}
