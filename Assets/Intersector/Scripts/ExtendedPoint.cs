﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ExtendedPoint
{
    /// <summary>
    /// координаты точки
    /// </summary>
    public Vector2 p;

    /// <summary>
    /// сипсок фигур, которым принедлежит точка
    /// </summary>
    public List<Figure> fs = new List<Figure>();
    /// <summary>
    /// индекс точки в соответствующей фигуре
    /// </summary>
    public List<int> indexes = new List<int>();
    /// <summary>
    /// список точек связанных с данной точкой ребрами после проведения триангуляции
    /// </summary>
    public List<ExtendedPoint> connectedPoints = new List<ExtendedPoint>();

    public ExtendedPoint(Vector2 p, Figure f, int index)
    {
        this.p = p;
        this.fs.Add(f);
        this.indexes.Add(index);
    }

    /// <summary>
    /// добавляем информацию о том, что данная точка принедлежит еще одной фигуре
    /// </summary>
    /// <param name="f"></param>
    /// <param name="index"></param>
    public void AddFigureInfo(Figure f, int index)
    {
        fs.Add(f);
        indexes.Add(index);
    }
}