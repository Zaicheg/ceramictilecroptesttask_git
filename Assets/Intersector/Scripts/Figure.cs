﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

/// <summary>
/// тип пересечения фигур
/// </summary>
public enum FiguresIntersectionType
{
    INTERSECTED,
    F1_OUTSIDE_F2,
    F1_INSIDE_F2,
    F2_INSIDE_F1
}

/// <summary>
/// Фигура - геометрическое отображение окна, стены, плитки
/// </summary>
public class Figure
{
    /// <summary>
    /// набор точек фигуры
    /// 1. считаем, что соседние точки соединены друг с другом
    /// 2. не должно быть самопересечений у отрезков фигуры
    /// 3. последняя точка соединена еще и с первой
    /// </summary>
    public List<Vector2> points = new List<Vector2>();

    /// <summary>
    /// временная структура для доабвления добавочных точек при поиске пересечений с другими фигурами
    /// </summary>
    private List<List<Vector2>> additionalPoints = new List<List<Vector2>>();

    public Figure()
    {
    }

    public Figure(List<Vector2> newPoints)
    {
        points = newPoints;
        RenewAdditionalPoints();
    }

    private void RenewAdditionalPoints()
    {
        additionalPoints = new List<List<Vector2>>();
        for (int i = 0; i < points.Count; i++)
        {
            additionalPoints.Add(new List<Vector2>());
        }
    }

    public void AddAdditionalPoint(int originalIndex, Vector2 point)
    {
        /*
        while(originalIndex >= additionalPoints.Count)
        {
            additionalPoints.Add(new List<Vector2>());
        } 
        */
        if (additionalPoints[originalIndex] == null)
        {
            additionalPoints[originalIndex] = new List<Vector2>();
        }

        additionalPoints[originalIndex].Add(point);
    }

    /// <summary>
    /// Пересекаем две фигуры
    /// 1. добавляем точки пересечения в список точек фигур
    /// 2. возвращаем тип пересечения фигур
    /// </summary>
    /// <param name="f1"></param>
    /// <param name="f2"></param>
    /// <returns></returns>
    public static FiguresIntersectionType Intersect(Figure f1, Figure f2)
    {
        int intersectionsCount = 0;
        for (int i = 0; i < f1.points.Count - 1; i++)
        {
            intersectionsCount += AddPointsInersect(f1, f2, i, i + 1);
        }
        intersectionsCount += AddPointsInersect(f1, f2, f1.points.Count - 1, 0);

        if (intersectionsCount == 0)
        {
            if (f1.IsInside(f2))
            {
                return FiguresIntersectionType.F1_INSIDE_F2;
            }
            else if (f2.IsInside(f1))
            {
                return FiguresIntersectionType.F2_INSIDE_F1;
            }
            else
            {
                return FiguresIntersectionType.F1_OUTSIDE_F2;
            }
        }

        f1.MergePoints();
        f2.MergePoints();

        return FiguresIntersectionType.INTERSECTED;
    }

    /// <summary>
    /// проверяем, находится ли наша фигура внутри другой
    /// </summary>
    /// <param name="f2"></param>
    /// <returns></returns>
    private bool IsInside(Figure f2)
    {
        foreach (var p in points)
        {
            //если хоть одна точка находится вне другой фигуры - наша фигура не может находиться внутри второй
            if (!f2.ContainsPoint(p))
            {
                return false;
            }
        }
        return true;
    }

    /// <summary>
    /// переносим доп точки в основные после пересечения фигур
    /// </summary>
    private void MergePoints()
    {
        List<Vector2> newPoints = new List<Vector2>();
        for (int i = 0; i < points.Count; i++)
        {
            newPoints.Add(points[i]);
            if (additionalPoints[i] != null)
            {
                if (additionalPoints[i].Count > 0)
                {
                    if (additionalPoints[i].Count > 1)
                    {
                        PointsOnLineComparer cmp = new PointsOnLineComparer(points[i]);
                        additionalPoints[i].Sort(cmp);
                    }
                    newPoints.AddRange(additionalPoints[i]);
                }
            }
        }
        points = newPoints;
        RenewAdditionalPoints();
    }

    /// <summary>
    /// вспомогательный класс для сортировки точек по расстоянию относительно заданной точки
    /// </summary>
    public class PointsOnLineComparer : Comparer<Vector2>
    {
        public Vector2 startPoint;
        public PointsOnLineComparer()
        {
        }

        public PointsOnLineComparer(Vector2 startPoint)
        {
            this.startPoint = startPoint;
        }

        public override int Compare(Vector2 x, Vector2 y)
        {
            float xl = (x - startPoint).SqrMagnitude();
            float yl = (y - startPoint).SqrMagnitude();
            return xl.CompareTo(yl);
        }
    }

    /// <summary>
    /// добавляем точки пересечения между отрезком f1[index1]-f1[index2] и любыми отрезками f2
    /// </summary>
    /// <param name="f1"></param>
    /// <param name="f2"></param>
    /// <param name="index1">индекс начала отрезка в f1</param>
    /// <param name="index2">индекс конца отрезка в f1</param>
    /// <returns></returns>
    private static int AddPointsInersect(Figure f1, Figure f2, int index1, int index2)
    {
        int intersectionsCount = 0;
        //Debug.Log(string.Format("i1={0}, i2={1}, count = {2}", index1, index2, f1.points.Count));
        Vector2 A = f1.points[index1];
        Vector2 B = f1.points[index2];
        //res.Add(A);

        //List<Vector2> tempToAdd = new List<Vector2>();
        //if (newPointsCol1)
        for (int j = 0; j < f2.points.Count - 1; j++)
        {
            Vector2 C = f2.points[j];
            Vector2 D = f2.points[j + 1];
            intersectionsCount = IntersectFiguresSegments(f1, f2, index1, intersectionsCount, A, B, j, C, D);
        }

        if (Utils.AreIntersects(A, B, f2.points[f2.points.Count - 1], f2.points[0]))
        {
            Vector2 C = f2.points[f2.points.Count - 1];
            Vector2 D = f2.points[0];
            intersectionsCount = IntersectFiguresSegments(f1, f2, index1, intersectionsCount, A, B, f2.points.Count - 1, C, D);

            /*
            Vector2 intersection = Utils.Intersection(A, B, f2.points[f2.points.Count - 1], f2.points[0]);
            f1.AddAdditionalPoint(index1, intersection);
            f2.AddAdditionalPoint(f2.points.Count - 1, intersection);
            intersectionsCount++;
            */
        }
        return intersectionsCount;
    }

    /// <summary>
    /// проверяем пересекаются ли конкретные 2 сегметна 2х фигур
    /// </summary>
    /// <param name="f1"></param>
    /// <param name="f2"></param>
    /// <param name="index1"></param>
    /// <param name="intersectionsCount"></param>
    /// <param name="A">начало отрезка фигуры f1</param>
    /// <param name="B">конец отрезка фигуры f1</param>
    /// <param name="j"></param>
    /// <param name="C">начало отрезка фигуры f2</param>
    /// <param name="D">конец отрезка фигуры f2</param>
    /// <returns></returns>
    private static int IntersectFiguresSegments(Figure f1, Figure f2, int index1, int intersectionsCount, Vector2 A, Vector2 B, int j, Vector2 C, Vector2 D)
    {
        if (Utils.AreIntersects(A, B, C, D))
        {
            Vector2 intersection = Utils.Intersection(A, B, C, D);
            f1.AddAdditionalPoint(index1, intersection);
            f2.AddAdditionalPoint(j, intersection);
            intersectionsCount++;
        }
        else
        {
            var crTypeA = Utils.ClassifyPointOnSegment(A, C, D);
            var crTypeB = Utils.ClassifyPointOnSegment(B, C, D);
            var crTypeC = Utils.ClassifyPointOnSegment(C, A, B);
            var crTypeD = Utils.ClassifyPointOnSegment(D, A, B);
            if (crTypeA == PointOnSegment.BETWEEN)
            {
                f2.AddAdditionalPoint(j, A);
            }
            if (crTypeB == PointOnSegment.BETWEEN)
            {
                f2.AddAdditionalPoint(j, B);
            }
            if (crTypeC == PointOnSegment.BETWEEN)
            {
                f1.AddAdditionalPoint(index1, C);
            }
            if (crTypeD == PointOnSegment.BETWEEN)
            {
                f1.AddAdditionalPoint(index1, D);
            }
        }

        return intersectionsCount;
    }

    /// <summary>
    /// проверяем содержит ли фигура точку внутри себя, через учет числа пересечений
    /// https://ru.wikipedia.org/wiki/%D0%97%D0%B0%D0%B4%D0%B0%D1%87%D0%B0_%D0%BE_%D0%BF%D1%80%D0%B8%D0%BD%D0%B0%D0%B4%D0%BB%D0%B5%D0%B6%D0%BD%D0%BE%D1%81%D1%82%D0%B8_%D1%82%D0%BE%D1%87%D0%BA%D0%B8_%D0%BC%D0%BD%D0%BE%D0%B3%D0%BE%D1%83%D0%B3%D0%BE%D0%BB%D1%8C%D0%BD%D0%B8%D0%BA%D1%83
    /// </summary>
    /// <param name="trCenter"></param>
    /// <returns></returns>
    public bool ContainsPoint(Vector2 trCenter)
    {
        int crosses = 0;
        Vector2 pLuch = new Vector2(trCenter.x, trCenter.y + 500f);
        for (int i = 0; i < points.Count - 1; i++)
        {
            if (Utils.AreIntersects(trCenter, pLuch, points[i], points[i + 1]))
            {
                crosses++;
            }
            else
            {
                //если пересечения нет, надо проверить, не проходит ли луч через точку фигуры
                crosses = CheckIfPointsOnLuch(trCenter, crosses, pLuch, i, i+1);
            }
        }
        if (Utils.AreIntersects(trCenter, pLuch, points[points.Count - 1], points[0]))
        {
            crosses++;
        }
        else
        {
            //если пересечения нет, надо проверить, не проходит ли луч через точку фигуры
            crosses = CheckIfPointsOnLuch(trCenter, crosses, pLuch, points.Count - 1, 0);
        }
        //если пересечений четное количество, то точка снаружи фигуры, если нечетное - то внутри
        return crosses % 2 == 1;
    }

    /// <summary>
    /// проверяем, не находится ли одна из точек фигуры на луче
    /// </summary>
    /// <param name="trCenter"></param>
    /// <param name="crosses"></param>
    /// <param name="pLuch"></param>
    /// <param name="i1"></param>
    /// <param name="i2"></param>
    /// <returns></returns>
    private int CheckIfPointsOnLuch(Vector2 trCenter, int crosses, Vector2 pLuch, int i1, int i2)
    {
        var crType = Utils.ClassifyPointOnSegment(points[i1], trCenter, pLuch);
        if (crType == PointOnSegment.BETWEEN || crType == PointOnSegment.ORIGIN || crType == PointOnSegment.DESTINATION)
        {
            //если точка фигуры на луче, то надо сдвинуть точку вправо и проверить пересечение заново
            //https://ru.wikipedia.org/wiki/%D0%97%D0%B0%D0%B4%D0%B0%D1%87%D0%B0_%D0%BE_%D0%BF%D1%80%D0%B8%D0%BD%D0%B0%D0%B4%D0%BB%D0%B5%D0%B6%D0%BD%D0%BE%D1%81%D1%82%D0%B8_%D1%82%D0%BE%D1%87%D0%BA%D0%B8_%D0%BC%D0%BD%D0%BE%D0%B3%D0%BE%D1%83%D0%B3%D0%BE%D0%BB%D1%8C%D0%BD%D0%B8%D0%BA%D1%83
            if (Utils.AreIntersects(trCenter, pLuch, points[i1] + Vector2.right * 0.001f, points[i2]))
            {
                crosses++;
            }
        }
        var crType2 = Utils.ClassifyPointOnSegment(points[i2], trCenter, pLuch);
        if (crType2 == PointOnSegment.BETWEEN || crType2 == PointOnSegment.ORIGIN || crType2 == PointOnSegment.DESTINATION)
        {
            if (Utils.AreIntersects(trCenter, pLuch, points[i1], points[i2] + Vector2.right * 0.001f))
            {
                crosses++;
            }
        }

        return crosses;
    }
}
