﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Triangle
{
    /// <summary>
    /// координаты треугольника
    /// </summary>
    public ExtendedPoint A, B, C;

    public Triangle(ExtendedPoint A, ExtendedPoint B, ExtendedPoint C)
    {
        this.A = A;
        this.B = B;
        this.C = C;
    }

    /// <summary>
    /// получаем центр треугольника
    /// </summary>
    /// <returns></returns>
    public Vector2 GetCenter()
    {
        return (A.p + B.p + C.p) / 3f;
    }

    /// <summary>
    /// проверяем, не является ли треугольник вырожденным (все 3 точки на одной прямой)
    /// </summary>
    /// <returns></returns>
    public bool IsVirozhden()
    {
        return Utils.IsVirozhdenTriangle(A.p, B.p, C.p);       
    }

    /// <summary>
    /// проверяем, находится ли точка внутри треугольника
    /// </summary>
    /// <param name="extendedPoint"></param>
    /// <returns></returns>
    public bool ContainsPoint(ExtendedPoint extendedPoint)
    {
        var cpA = Utils.ClassifyPointOnSegment(extendedPoint.p, A.p, B.p);
        var cpB = Utils.ClassifyPointOnSegment(extendedPoint.p, B.p, C.p);
        var cpC = Utils.ClassifyPointOnSegment(extendedPoint.p, C.p, A.p);
        if (( OnSideOrOnSegment(cpA, PointOnSegment.LEFT) 
            && OnSideOrOnSegment(cpB, PointOnSegment.LEFT)
            && OnSideOrOnSegment(cpC, PointOnSegment.LEFT))
            || (OnSideOrOnSegment(cpA, PointOnSegment.RIGHT)
            && OnSideOrOnSegment(cpB, PointOnSegment.RIGHT)
            && OnSideOrOnSegment(cpC, PointOnSegment.RIGHT)))
        {
            return true;
        }
        return false;

        /*
        Vector2 p1 = extendedPoint.p;
        Vector2 p2 = new Vector2(p1.x, p1.y + 500f);            
        int intersects = 0;
        if (Utils.AreIntersects(p1, p2, A.p, B.p))
        {
            intersects++;
        }
        if (Utils.AreIntersects(p1, p2, A.p, C.p))
        {
            intersects++;
        }
        if (Utils.AreIntersects(p1, p2, B.p, C.p))
        {
            intersects++;
        }
        return intersects % 2 == 1;
        */
    }

    /// <summary>
    /// проверяем находится ли точка с заданной стороны или на отрезке
    /// </summary>
    /// <param name="cpA"></param>
    /// <param name="side"></param>
    /// <returns></returns>
    private bool OnSideOrOnSegment(PointOnSegment cpA, PointOnSegment side)
    {
        return cpA == side || cpA == PointOnSegment.ORIGIN || cpA == PointOnSegment.DESTINATION || cpA == PointOnSegment.BETWEEN;
    }
}

