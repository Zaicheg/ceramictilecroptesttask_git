﻿using UnityEngine;
using System.Collections;

public class ExtendedSegment
{
    /// <summary>
    /// точка А отрезка
    /// </summary>
    public ExtendedPoint A;
    /// <summary>
    /// точка B отрезка
    /// </summary>
    public ExtendedPoint B;
    /// <summary>
    /// индекс точки А в триангуляторе
    /// </summary>
    public int indexA;//
    /// <summary>
    /// индекс точки B в триангуляторе
    /// </summary>
    public int indexB;
    /// <summary>
    /// квадрат длиты отрезка
    /// </summary>
    public float sqrLength;

    public ExtendedSegment(ExtendedPoint A, ExtendedPoint B, int indexA, int indexB)
    {
        this.A = A;
        this.B = B;
        this.indexA = indexA;
        this.indexB = indexB;
        this.sqrLength = (this.A.p - this.B.p).SqrMagnitude();
    }
}
