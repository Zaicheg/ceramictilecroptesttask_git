﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public static class Utils
{
    private const float e = 0.000001f;

    /// <summary>
    /// проверяет левая тройка векторов или нет
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <param name="c"></param>
    /// <returns></returns>
    public static bool isLeft(Vector2 a, Vector2 b, Vector2 c)
    {
        float abX = b.x - a.x;
        float abY = b.y - a.y;
        float acX = c.x - a.x;
        float acY = c.y - a.y;

        return abX * acY - acX * abY < 0;
    }

    /// <summary>
    /// находится ли точка p внутри треугольника abc
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <param name="c"></param>
    /// <param name="p"></param>
    /// <returns></returns>
    public static bool isPointInside(Vector2 a, Vector2 b, Vector2 c, Vector2 p) 
    {
        float ab = (a.x - p.x) * (b.y - a.y) - (b.x - a.x) * (a.y - p.y);
        float bc = (b.x - p.x) * (c.y - b.y) - (c.x - b.x) * (b.y - p.y);
        float ca = (c.x - p.x) * (a.y - c.y) - (a.x - c.x) * (c.y - p.y);

        return (ab >= 0 && bc >= 0 && ca >= 0) || (ab <= 0 && bc <= 0 && ca <= 0);
    }

    /// <summary>
    /// возвращает точку пересечения отрезков
    /// </summary>
    /// <param name="A"></param>
    /// <param name="B"></param>
    /// <param name="C"></param>
    /// <param name="D"></param>
    /// <returns></returns>
    public static Vector2 Intersection(Vector2 A, Vector2 B, Vector2 C, Vector2 D)
    {
        float xo = A.x, yo = A.y;
        float p = B.x - A.x, q = B.y - A.y;

        float x1 = C.x, y1 = C.y;
        float p1 = D.x - C.x, q1 = D.y - C.y;

        float x = (xo * q * p1 - x1 * q1 * p - yo * p * p1 + y1 * p * p1) /
            (q * p1 - q1 * p);
        float y = (yo * p * q1 - y1 * p1 * q - xo * q * q1 + x1 * q * q1) /
            (p * q1 - p1 * q);
        //float z = (zo * q * r1 - z1 * q1 * r - yo * r * r1 + y1 * r * r1) /
        //(q * r1 - q1 * r);

        return new Vector2(x, y);
    }

    /// <summary>
    /// функция sign, но при 0 возвращает 0
    /// </summary>
    /// <param name="val"></param>
    /// <returns></returns>
    public static float Sign(float val)
    {
        return val < 0 ? -1 : (val > 0 ? 1 : 0);
        /*
        if (val < 0)
        {
            return -1f;
        }
        if (val> 0)
        {
            return 1f;
        }
        return 0f;
        */
    }

    /// <summary>
    /// проверяет пересекаются ли отрезки
    /// </summary>
    /// <param name="A"></param>
    /// <param name="B"></param>
    /// <param name="C"></param>
    /// <param name="D"></param>
    /// <returns></returns>
    public static bool AreIntersects(Vector2 A, Vector2 B, Vector2 C, Vector2 D)
    {
        float v1 = (D.x - C.x) * (A.y - C.y) - (D.y - C.y) * (A.x - C.x);
        float v2 = (D.x - C.x) * (B.y - C.y) - (D.y - C.y) * (B.x - C.x);
        float v3 = (B.x - A.x) * (C.y - A.y) - (B.y - A.y) * (C.x - A.x);
        float v4 = (B.x - A.x) * (D.y - A.y) - (B.y - A.y) * (D.x - A.x);
        return (((v1 < 0 && v2 > 0) || (v1 > 0 && v2 < 0))
            && ((v3 < 0 && v4 > 0) || (v3 > 0 && v4 < 0)));
        //return (Sign(v1) * Sign(v2) < 0f) && (Sign(v3) * Sign(v4) < 0f);
    }

    private static bool AreEqual(float v1, float v2, float e = 0.0001f)
    {
        return Mathf.Abs(v1 - v2) <= e;
    }

    /// <summary>
    /// проверяет вырожден ли треугольник
    /// </summary>
    /// <param name="A"></param>
    /// <param name="B"></param>
    /// <param name="C"></param>
    /// <returns></returns>
    public static bool IsVirozhdenTriangle(Vector2 A, Vector2 B, Vector2 C)
    {
        /*
        List<float> lengths = new List<float>(3);
        lengths.Add((A - B).magnitude);
        lengths.Add((B - C).magnitude);
        lengths.Add((A - C).magnitude);

        lengths.Sort();

        if (Mathf.Abs(lengths[0] + lengths[1] - lengths[2]) <= e)        
        {
            return true;
        }
        return false;
        */
        var clP = ClassifyPointOnSegment(A, B, C);
        return clP == PointOnSegment.ORIGIN 
            || clP == PointOnSegment.DESTINATION 
            || clP == PointOnSegment.BETWEEN
            || clP == PointOnSegment.BEHIND
            || clP == PointOnSegment.BEYOND;
    }

    /// <summary>
    /// содержит ли отрезок точку
    /// </summary>
    /// <param name="point"></param>
    /// <param name="A"></param>
    /// <param name="B"></param>
    /// <returns></returns>
    public static bool IsSegmentContainsPointV2(Vector2 point, Vector2 A, Vector2 B)
    {
        /*
        if (IsVirozhdenTriangle(point, A, B))
        {
            if (point.x >= Mathf.Min(A.x, B.x) - e && point.x <= Mathf.Max(A.x, B.x) + e
                && point.y >= Mathf.Min(A.y, B.y) - e && point.y <= Mathf.Max(A.y, B.y) + e)
            {
                return true;
            }
            return false;
        }
        return false;
        */
        var clP = ClassifyPointOnSegment(point, A, B);
        return clP == PointOnSegment.ORIGIN || clP == PointOnSegment.DESTINATION || clP == PointOnSegment.BETWEEN;
    }

    /// <summary>
    /// Классифицирует расположение точки относительно отрезка
    /// http://algolist.manual.ru/maths/geom/datastruct.php
    /// </summary>
    /// <param name="p0">segment point A</param>
    /// <param name="pl">segment point B</param>
    /// <param name="p2">point</param>
    /// <returns></returns>
    public static PointOnSegment ClassifyPointOnSegment(Vector2 point, Vector2 p0, Vector2 p1)
    {        
        Vector2 a = p1 - p0;
        Vector2 b = point - p0;
        double sa = a.x * b.y - b.x * a.y;
        if (sa > 0.0)
            return PointOnSegment.LEFT;
        if (sa < 0.0)
            return PointOnSegment.RIGHT;
        if ((a.x * b.x < 0.0) || (a.y * b.y < 0.0))
            return PointOnSegment.BEHIND;
        if (a.magnitude < b.magnitude)
            return PointOnSegment.BEYOND;
        if (p0 == point)
            return PointOnSegment.ORIGIN;
        if (p1 == point)
            return PointOnSegment.DESTINATION;
        return PointOnSegment.BETWEEN;
    }
}

/// <summary>
/// классифицирует расположение точки относительно отрезка
/// </summary>
public enum PointOnSegment
{
    LEFT, //в левой полуплоскости
    RIGHT,//в правой полуплоскости
    BEYOND, //на прямой впереди отрезка
    BEHIND, //на прямой позади отрезка
    BETWEEN, //на отрезке между началом и концом
    ORIGIN, //совпадает с начальной точкой
    DESTINATION //совпадает с конечной точкой
}