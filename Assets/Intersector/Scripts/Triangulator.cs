﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Triangulator
{
	#region Переменные класса

	/// <summary>
	/// фигура подрезаемой штуки
	/// </summary>
	public Figure thingFigure;
	/// <summary>
	/// фигура стены
	/// </summary>
	public Figure wallFigure;
	/// <summary>
	/// список фигур проемов в стене
	/// </summary>
	public List<Figure> holeFigures;

	/// <summary>
	/// базовые точки для триангуляции
	/// </summary>
	public List<Vector2> basePoints = new List<Vector2>();
	/// <summary>
	/// расширенные базовые точки для триангуляции
	/// </summary>
	public List<ExtendedPoint> baseExtPoints = new List<ExtendedPoint>();
	/// <summary>
	/// исходные отрезки - стороны фигур, которые должны остаться после триангуляции
	/// </summary>
	public List<ExtendedSegment> origSegments = new List<ExtendedSegment>();

	/// <summary>
	/// список всех возможных отрезков
	/// </summary>
	public List<ExtendedSegment> allOtherSegments;

	/// <summary>
	/// список отрезков, которые описывают триангуляцию
	/// </summary>
	public List<ExtendedSegment> triangleSegments;

	/// <summary>
	/// итог триангуляции - набор треугольников
	/// </summary>
	public List<Triangle> triangles;

	/// <summary>
	/// итоговый результат - набор треугольников, каждый из которых принадлежит плитке И стене, но НЕ принадлежит отверстиям
	/// </summary>
	public List<Triangle> crossedTriangles;

	#endregion

	#region Вызываемые извне методы и конструкторы

	/// <summary>
	/// подрезать плитки
	/// </summary>
	/// <param name="things"></param>
	/// <param name="wall"></param>
	/// <param name="holes"></param>
	/// <param name="thingWidth"></param>
	public void CropThings(List<Thing> things, Wall wall, List<Hole> holes, float thingWidth)
	{
		foreach (var cer in things)
		{
			///подрезать плитку
			CropThing(cer, wall, holes, thingWidth);
		}
	}

	/// <summary>
	/// подрезаем плитку
	/// </summary>
	/// <param name="thing"></param>
	/// <param name="wall"></param>
	/// <param name="holes"></param>
	/// <param name="thingWidth"></param>
	public void CropThing(Thing thing, Wall wall, List<Hole> holes, float thingWidth)
	{
		Profiler.BeginSample("CropTile");

		//=== Подготовка

		ClearAllData();

		//=== Создаем фигуры (плитки, стены, проёмов) по входным параметрам и фиксируем фигуры в переменных класса

		Figure cerFigure = new Figure(wall.ProjectPointsToWall(thing.GetOriginContourPoints()));
		Figure wallFigure = new Figure(wall.ProjectPointsToWall(wall.GetOriginContourPoints()));
		List<Figure> holeFiguresTemp = new List<Figure>();
		foreach (var h in holes)
		{
			holeFiguresTemp.Add(new Figure(wall.ProjectPointsToWall(h.GetOriginContourPoints())));
		}

		thingFigure = cerFigure;
		this.wallFigure = wallFigure;

		//=== Ищем пересечения фигур: плитки со стеной, плитки с проёмами, проёмов со стеной, проёмов с проёмами.
		//На основе этого модифицируем фигуры -- они теперь содержат как исходные точки, так и точки всех своих пересечений.
		//Чтобы жадный алгоритм триангуляции сработал правильно - надо пересечь все частвующие фигуры. 

		var intersectionWithWallType = Figure.Intersect(thingFigure, this.wallFigure);
		if (intersectionWithWallType == FiguresIntersectionType.F1_OUTSIDE_F2) //если плитка вне стены - просто отключаем ее и все
		{
			thing.SetThingEnabled(false);
			return;
		}

		// Вариант 1. Если плитка снаружи дырки, то дырка в дальгейших манипуляциях участия не принимает 
		// Вариант 2. Если плитка полностью внутри какой-то дырки, то просто отключаем плитку и на этом заканчиваем
		// Вариант 3. В противном случае добавляем дыру для дальнейшей триангуляции
		holeFigures = new List<Figure>();
		foreach (var hF in holeFiguresTemp)
		{
			var intersectionWithHoleType = Figure.Intersect(thingFigure, hF);
			if (intersectionWithHoleType == FiguresIntersectionType.F1_OUTSIDE_F2)
			{
				//если плитка снаружи дырки, то дырка в дальгейших манипуляциях участия не принимает 
				continue;
			}
			else if (intersectionWithHoleType == FiguresIntersectionType.F1_INSIDE_F2)
			{
				// если плитка полностью внутриы какой-то дырки, то просто отключаем плитку и на этом заканчиваем
				thing.SetThingEnabled(false);
				return;
			}
			//в противном случае добавляем дыру для дальнейшей триангуляции
			holeFigures.Add(hF);
		}

		// Если нет пересечений с дрыками и плитка полностью внутри стены - никакие манипуляции не требуются
		if (holeFigures.Count == 0 && intersectionWithWallType == FiguresIntersectionType.F1_INSIDE_F2)
		{
			//нет пересечений с дрыками и плитка полностью внутри стены - никакие манипуляции не требуются
			thing.SetThingFullSize();
			return;
		}

		//Debug.Log("Holes Count = "+holeFigures.Count.ToString());

		//чтобы жадный алгоритм триангуляции сработал правильно - надо пересечь все частвующие фигуры. 
		//плитку со стеной и дырами уже пересекли. 
		//осталось пересечь дыры друг с другом        
		IntersectAllHoleFigures();
		//и стену со всеми дырами
		IntersectWallWithHoles();

		//=== После нахождения всех пересечений создаем список всех входных точек и отрезков

		AddBasePoints(thingFigure);
		AddBasePoints(this.wallFigure);
		foreach (var hF in holeFigures)
		{
			AddBasePoints(hF);
		}

		//=== Запускаем триангуляцию. На выходе получим список треугольников, из которых выберем только подходящие

		Triangulate();
		CrossTriangles();

		//=== Строим меш плитки используя полученный спиок треугольников

		thing.CutThing(crossedTriangles, wall, thingWidth);

		/*всякий дебаг
        GameObject goTr = new GameObject("TempRes");                
        foreach (var t in triangles)
        {
            GameObject tr = new GameObject("tr");
            tr.transform.SetParent(goTr.transform, true);            

            var pA = new GameObject();
            pA.transform.position = wall.TransformPoint(t.A.p);
            pA.transform.SetParent(tr.transform, true);
            var pB = new GameObject();
            pB.transform.position = wall.TransformPoint(t.B.p);
            pB.transform.SetParent(tr.transform, true);
            var pC = new GameObject();
            pC.transform.position = wall.TransformPoint(t.C.p);
            pC.transform.SetParent(tr.transform, true);

            tr.AddComponent<PointsHolder>();
        }


        var goP = new GameObject("Points");
        foreach(var p in baseExtPoints)
        {
            GameObject pointGO = new GameObject("PointGO");
            pointGO.transform.position = wall.TransformPoint(p.p);
            pointGO.transform.SetParent(goP.transform, true);
            foreach (var pc in p.connectedPoints)
            {
                GameObject pointGOCon = new GameObject("ConnectedPointGO");
                pointGOCon.transform.position = wall.TransformPoint(pc.p);
                pointGOCon.transform.SetParent(pointGO.transform, true);
            }
        }
        */

		/*
        foreach (var t in wallFigure.points)
        {
            var pt1 = GameObject.CreatePrimitive(PrimitiveType.Cube);
            pt1.transform.position = wall.TransformPoint(t);
            pt1.transform.SetParent(go.transform, true);          
        }
        */

		Profiler.EndSample();
	}

	/// <summary>
	/// Основной конструктор -- пустой
	/// </summary>
	public Triangulator()
	{
	}

	/// <summary>
	/// Конструктор, используемый в скрипте Demo
	/// </summary>
	/// <param name="f1"></param>
	/// <param name="f2"></param>
	public Triangulator(Figure f1, Figure f2)
	{
		this.thingFigure = f1;
		this.wallFigure = f2;
		AddBasePoints(f1);
		AddBasePoints(f2);
	}

	#endregion

	#region Основные методы, в порядке вызова

	/// <summary>
	/// пересекаем стену и проемы для жадного алгоритма
	/// </summary>
	private void IntersectWallWithHoles()
	{
		foreach (var hF in holeFigures)
		{
			Figure.Intersect(wallFigure, hF);
		}
	}

	/// <summary>
	/// пересекаем фигуры проемов друг с другом для жадного алгоритма
	/// </summary>
	private void IntersectAllHoleFigures()
	{
		for (int i = 0; i < holeFigures.Count - 1; i++)
		{
			for (int j = i + 1; j < holeFigures.Count; j++)
			{
				Figure.Intersect(holeFigures[i], holeFigures[j]);
			}
		}
	}

	/// <summary>
	/// добавляем точки фигуры в список точек и отрезки фигуры в список орезков.
	/// при этом совпадающие точки должны отображаться одной сущностью
	/// </summary>
	/// <param name="figure"></param>
	private void AddBasePoints(Figure figure)
	{
		ExtendedPoint lastExtPoint = null;
		ExtendedPoint firstExtPoint = null;
		for (int i = 0; i < figure.points.Count; i++)
		{
			if (!basePoints.Contains(figure.points[i]))
			{
				basePoints.Add(figure.points[i]);
				ExtendedPoint newP = new ExtendedPoint(figure.points[i], figure, i);
				baseExtPoints.Add(newP);

				if (i > 0)
				{
					origSegments.Add(new ExtendedSegment(lastExtPoint, newP, baseExtPoints.IndexOf(lastExtPoint), baseExtPoints.IndexOf(newP)));
				}
				if (i == 0)
				{
					firstExtPoint = newP;
				}
				lastExtPoint = newP;
			}
			else
			{
				ExtendedPoint oldP = baseExtPoints[basePoints.IndexOf(figure.points[i])];
				oldP.AddFigureInfo(figure, i);
				if (i > 0)
				{
					origSegments.Add(new ExtendedSegment(lastExtPoint, oldP, baseExtPoints.IndexOf(lastExtPoint), baseExtPoints.IndexOf(oldP)));
				}
				if (i == 0)
				{
					firstExtPoint = oldP;
				}
				lastExtPoint = oldP;
			}
		}

		// Добавляем отрезок между последней точкой и первой

		origSegments.Add(new ExtendedSegment(lastExtPoint, firstExtPoint, baseExtPoints.IndexOf(lastExtPoint), baseExtPoints.IndexOf(firstExtPoint)));
	}

	/// <summary>
	/// производим триангуляцию
	/// </summary>
	public void Triangulate()
	{
		//=== Создаем список всех сегментов

		FillAllsegments(baseExtPoints.Count, ref origSegments, ref allOtherSegments);

		//=== Создаём список сегментов, из которых сформируются треугольники. 
		// Для этого берём сегменты, полученные в FillAllsegments и наззодим те, которые не пересекают друг друга

		FindTriangleSegments();

		//=== Зная список сегментов для будущих треугольников, определяем, какие точки с какими точками соединены в рамках их треугольников.

		UpdateConnectionsInfo();

		//=== На основе: списка точек, связей между точками, списка сегментов треугольников -- получаем список треугольников
		// В полученных треугольниках убираем те, которые являются вырожденными

		FillTriangles();
		//Debug.Log(string.Format("trCount={0}, bEPointsCount = {1}", triangles.Count, baseExtPoints.Count));
		ClearVirozhdenTriangles();
	}

	/// <summary>
	/// отбираем из всех треугольников только те, которые принадлежат плитке И стене и НЕ принадлежат отверстиям
	/// </summary>
	private void CrossTriangles()
	{
		crossedTriangles = new List<Triangle>();
		foreach (var tr in triangles)
		{
			Vector2 trCenter = tr.GetCenter();
			if (thingFigure.ContainsPoint(trCenter) && wallFigure.ContainsPoint(trCenter)
				&& !AnyHoleContainsPoint(trCenter))
			{
				crossedTriangles.Add(tr);
			}
		}
	}

	#endregion

	#region Вспомогательные методы -- формирование контента для алгоритма

	/// <summary>
	/// заполняем массив треугольников по имеющимся отрезкам триангуляции
	/// </summary>
	private void FillTriangles()
	{
		triangles = new List<Triangle>();
		List<ExtendedPoint> wastedPoints = new List<ExtendedPoint>();
		foreach (var p in baseExtPoints)
		{
			List<ExtendedPoint> connectedPoints = p.connectedPoints;

			for (int i = 0; i < connectedPoints.Count - 1; i++)
			{
				if (wastedPoints.Contains(connectedPoints[i]))
				{
					continue;
				}
				for (int j = i + 1; j < connectedPoints.Count; j++)
				{
					if (wastedPoints.Contains(connectedPoints[j]))
					{
						continue;
					}
					if (connectedPoints[i].connectedPoints.Contains(connectedPoints[j])
						|| connectedPoints[j].connectedPoints.Contains(connectedPoints[i]))
					{
						triangles.Add(new Triangle(p, connectedPoints[i], connectedPoints[j]));
					}
				}
			}
			wastedPoints.Add(p);
		}
	}

	/// <summary>
	/// находим те отрезки, которые состявят список триангуляции методом жадного алгоритма     
	/// </summary>
	private void FindTriangleSegments()
	{
		//сортируем список всех отрезков по длине для жадного алгоритма триангуляции
		allOtherSegments.Sort(new SegmentsComparer());

		triangleSegments = new List<ExtendedSegment>();
		triangleSegments.AddRange(origSegments);//исходные отрезки помещаем сразу в список отрезков

		///и добавляем все отрезки, которые не пересекают уже существующие (суть жадного алгоритма)
		foreach (var seg in allOtherSegments)
		{
			if (!Intersects(seg, triangleSegments))
			{
				triangleSegments.Add(seg);
			}
		}
	}

	/// <summary>
	/// заполняем список всех возможных отрезков (кроме тех, что уже заданы сторонами фигур)
	/// </summary>
	/// <param name="pCount"></param>
	/// <param name="origSegments"></param>
	/// <param name="allOtherSegments"></param>
	private void FillAllsegments(int pCount, ref List<ExtendedSegment> origSegments, ref List<ExtendedSegment> allOtherSegments)
	{
		allOtherSegments = new List<ExtendedSegment>(pCount * pCount);

		for (int i = 0; i < pCount - 1; i++)
		{
			for (int j = i + 1; j < pCount; j++)
			{
				var origSeg = origSegments.Find(x => (x.indexA == i && x.indexB == j) || (x.indexB == i && x.indexA == j));
				if (origSeg == null)
				{
					allOtherSegments.Add(new ExtendedSegment(baseExtPoints[i], baseExtPoints[j], i, j));
				}
			}
		}
	}

	/// <summary>
	/// составляем список точек, которые связаны отрезками после триангуляции
	/// </summary>
	private void UpdateConnectionsInfo()
	{
		foreach (var p in baseExtPoints)
		{
			List<ExtendedPoint> connectedPoints = new List<ExtendedPoint>();
			foreach (var seg in triangleSegments)
			{
				if (seg.A == p)
				{
					connectedPoints.Add(seg.B);
				}
				else if (seg.B == p)
				{
					connectedPoints.Add(seg.A);
				}
			}
			p.connectedPoints = connectedPoints;
		}
	}

	#endregion

	#region Вспомогательные методы - проверки на пересечения, на корректность, очистки списков от лишнего

	/// <summary>
	/// убираем вырожденные треугольники
	/// </summary>
	private void ClearVirozhdenTriangles()
	{
		for (int i = triangles.Count - 1; i >= 0; i--)
		{
			if (triangles[i].IsVirozhden())
			{
				triangles.RemoveAt(i);
			}
			else
			{

				for (int j = 0; j < baseExtPoints.Count; j++)
				{
					//Debug.Log(string.Format("i={0}, j={1}", i, j));
					if (baseExtPoints[j] != triangles[i].A
						&& baseExtPoints[j] != triangles[i].B
						&& baseExtPoints[j] != triangles[i].C)
					{
						if (triangles[i].ContainsPoint(baseExtPoints[j]))
						{
							triangles.RemoveAt(i);
							break;
						}
					}
				}

			}
		}
	}

	/// <summary>
	/// провеяем содержит ли указанную точку хоть один проем стены
	/// </summary>
	/// <param name="point"></param>
	/// <returns></returns>
	private bool AnyHoleContainsPoint(Vector2 point)
	{
		foreach (var hF in holeFigures)
		{
			if (hF.ContainsPoint(point))
			{
				return true;
			}
		}
		return false;
	}

	/// <summary>
	/// проверяем, пересекается ли заданный отрезок с каким либоотрезком из списка
	/// </summary>
	/// <param name="seg"></param>
	/// <param name="triangleSegments"></param>
	/// <returns></returns>
	public bool Intersects(ExtendedSegment seg, List<ExtendedSegment> triangleSegments)
	{
		int tSegCount = triangleSegments.Count;
		for (int i = 0; i < tSegCount; i++)
		{
			if (Utils.AreIntersects(seg.A.p, seg.B.p, triangleSegments[i].A.p, triangleSegments[i].B.p))
			{
				return true;
			}

			else if (Utils.IsSegmentContainsPointV2(triangleSegments[i].A.p, seg.A.p, seg.B.p)
				&& Utils.IsSegmentContainsPointV2(triangleSegments[i].B.p, seg.A.p, seg.B.p))
			{
				return true;
			}

		}
		return false;
	}

	#endregion

	#region Вспомогательные методы - разное

	/// <summary>
	/// зачищаем все данные от предыдущей подрезки плитки
	/// </summary>
	private void ClearAllData()
	{
		thingFigure = null;
		wallFigure = null;
		holeFigures = new List<Figure>();

		basePoints = new List<Vector2>();
		baseExtPoints = new List<ExtendedPoint>();
		origSegments = new List<ExtendedSegment>();

		allOtherSegments = new List<ExtendedSegment>();

		triangleSegments = new List<ExtendedSegment>();

		triangles = new List<Triangle>();

		crossedTriangles = new List<Triangle>();
	}

	/// <summary>
	/// класс для сортировки отрезков по длине
	/// </summary>
	public class SegmentsComparer : Comparer<ExtendedSegment>
	{
		public override int Compare(ExtendedSegment x, ExtendedSegment y)
		{
			return x.sqrLength.CompareTo(y.sqrLength);
		}
	}

	#endregion
}
