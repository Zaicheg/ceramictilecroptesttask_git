﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[ExecuteInEditMode]
public class PointsHolder : MonoBehaviour {

    public List<Transform> points;

    public Color color = Color.cyan;

    public bool init = false;
	// Use this for initialization
	void Start ()
    {
        InitIfNeeded();
	}

    public void OnDrawGizmos()
    {
        InitIfNeeded();
        Gizmos.color = color;
        for (int i =0;i<points.Count-1;i++)
        {
            if (points[i] == null)
            {
                init = false;
                InitIfNeeded();
                return;
            }
            Gizmos.DrawLine(points[i].position, points[i+1].position);
            //Gizmos.DrawWireCube(points[i].position, Vector3.one * 0.1f);
        }
        Gizmos.DrawLine(points[points.Count - 1].position, points[0].position);
        //Gizmos.DrawWireCube(points[points.Count - 1].position, Vector3.one * 0.1f);
        Gizmos.color = Color.white;
    }

    public void OnDrawGizmosSelected()
    {
        InitIfNeeded();
        Gizmos.color = Color.red;
        for (int i = 0; i < points.Count - 1; i++)
        {
            if (points[i] == null)
            {
                init = false;
                InitIfNeeded();
                return;
            }
            Gizmos.DrawLine(points[i].position, points[i + 1].position);
            Gizmos.DrawWireCube(points[i].position, Vector3.one * 0.1f);
        }
        Gizmos.DrawLine(points[points.Count - 1].position, points[0].position);
        Gizmos.DrawWireCube(points[points.Count - 1].position, Vector3.one * 0.1f);
        Gizmos.color = Color.white;
    }

    private void InitIfNeeded()
    {
        if (init) return;
        points = new List<Transform>();
        var pTemp = GetComponentsInChildren<Transform>();
        foreach (var p in pTemp)
        {
            if (p != transform)
            {
                points.Add(p);
            }
        }
        init = true;
    }
}
